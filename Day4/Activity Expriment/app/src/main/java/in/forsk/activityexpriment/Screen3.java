package in.forsk.activityexpriment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Screen3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen3);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
